# UA Basic Page *(now deprecated: use [UAQS Page](https://bitbucket.org/ua_drupal/uaqs_page) instead)*

Provides basic page content type.

## Features

- Provides 'ua_page' content type (clone of Basic page content type from Drupal Standard install profile).
